# 1. Explain how object oriented programming works with a through understanding of the keyword THIS and the NEW keyword

object oriented programming adalah sebuah paradigma pemrograman dengan menggunakan objek sebagai analoginya. contoh yang biasa digunakan adalah menganggap binatang sebagai objek. setiap binatang memiliki atribut berupa nama, cara berkembang biaknya, jumlah kaki, dan masih banyak atribut lainnya yang dapat disebutkan. didalam object oriented programming sendiri terdapat sistem yang dinamakan enkapsulasi yang berarti setiap object / class dapat memiliki sebuah atribut ataupun metode.
di dalam OOP sendiri juga terdapat berbagai modifier seperti public, protected, dan juga private agar tidak sembarang objek lain dapat mengakses baik atribute maupun metode yang dimiliki objek tersebut.
Keyword THIS sendiri merujuk kepada objek itu sendiri, seperti contohnya pada kode dibawah ini, objek `animal` memanggil nama yang dia miliki:

```JS
class Animal{
    private name: string;

    public callMyName(){
        return this.name;
    }
}
```

sedangkan keyword NEW mendandakan pembuatan objek baru. contohnya saja seperti ini:

```JS
class Animal{
    private name: string;

    constructor(name: string){
        this.name = name;
    }
}

const zebra = new Animal('zebra');
const gajah = new Animal('gajah');
```

berarti ada 2 objek binatang yang terbuat pada kode diatas, yaitu gajah dan juga zebra

# 2. What is the new class syntax and how to create instance method, class methods

new class syntax digunakan untuk membuat objek / instance baru.
contohnya adalah seperti berikut:

```JS
class Animal{
    private name: string;
    private sound: string;
    constructor(name: string, voice: string){
        this.name = name;
        this.sound = voice;
    }
    public function talk(){
        return this.sound;
    }
}
const anjing = new Animal('anjing', 'guk');
const monyet = new Animal('monyet', 'uuaa');

anjing.talk() //guk
monyet.talk() //uuaa
```

# 3. Give an example of how to implement inheritance in ES2015 using extends and super

```JS
import React, {Component} from 'react';

class ComponentOne extends Component<{},{}>{
    constructor(props){
        super(props);
    }
}
```

extends diatas menandakan bahwa class ComponentOne mengambil beberapa attribute/metode yang diwarisi oleh class Component milik React.
sedangkan untuk super(props) sendiri menandakan bahwa nantinya ketika kita melakukan koding kita dapat mengambil attribute props ini dengan menggunakan keyword THIS

# 4. Imagine refactoring an ES5 application to ES2015, how would you go about it?

To be honest i've never used ES5 to code before, so i dont really know what will i do to refactor the project code. maybe i will just refactor the code that i've never seen or that can be improved in ES6 like spread operator, `import` instead of `require`, and all the feature that is available in ES6.

# 5. Give an example of how you structure applications with design pattern using closure and modules

```JS
import addFive from './addFive';
const usiaAyah = 55;

function testUmur1 (umur1) {
    const usiaBunda = 51

    function testUmur2 (umur2){
        const usiaKakek = 80;

        console.log(`usia ayah = ${addFive(usiaAyah)}`);
        console.log(`usia bunda  = ${addFive(usiaBunda)}`);
        console.log(`usia kakek = ${addFive(usiaKakek)}`)
        console.log(`umur1 = ${addFive(umur1)}`);
        console.log(`umur2 = ${addFive(umur2)}`);
    }

    testUmur2(20);
}
testUmur1(25);
```

# 6. What are your preffered ways of testing your web application?

Since i've never used jest or cypress or enzyme before, all of this time i leave all the testing process to the tester (yang pastinya sudah ditest manual oleh diri saya terlebih dahulu). But ya seharusnya dilakukan unit testing secara manual terlebih dahulu

# 7. Which web server do you use? why? Explain pros and cons of your choice

Saya menggunakan apache web server. saya menggunakan apache karena gratis dan sudah terbiasa dari dulu menggunakan web server ini. Ditambah lagi saya belum pernah mencoba menggunakan web server lainnya seperti nginx. di apache ada fitur berupa `.htaccess` yang dapat merouting secara custom apa yang ingin kita routing. untuk cons nya sendiri mungkin apache sudah terlalu jadul jadi seperti lebih ketinggalan teknologinya

# 8. What is your preffered production deployment process

From dev env => test env => UAT env => Live Env.
my preffered production is actually using a CI/CD on gitlab and like me and my team usually do, kita bakal discuss repository mana yang harus naik terlebih dahulu contohnya repository Back End harus naik terlebih dahulu agar tidak menyebabkan error atau tampilan yang tidak dinginkan karena tidak adanya data yang disediakan oleh backend.

# 9. Give an example of clean README.md documentation

https://gitlab.com/agungchandranata/ajaibtest/-/blob/agung/README.md. Well basically the default CRA README.md is already clean enough for me. There's a way how to run the project in dev env, how to test the env, and how to build it, the documentation link and another info that is useful for us the developers to run the project smoothly.
