import { BASE_URL } from "../constant";
import { UserFetchResponse } from "../interfaces/User";

class UserService {
  public static async getList(params?: string): Promise<UserFetchResponse> {
    const fetchResponse = await fetch(`${BASE_URL}${params ? params : ""}`, {
      method: "GET",
    });

    const response: UserFetchResponse = await fetchResponse.json();
    // if (response.error) {
    //   throw new Error(response.error);
    // }
    if (response.results) {
      return response;
    }
    throw new Error("Abnormal response");
  }
}

export default UserService;
