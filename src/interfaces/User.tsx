import { E_GENDER } from "../constant";

export interface DataUser {
  username: string;
  name: string;
  email: string;
  gender: E_GENDER;
  date: string;
}

export interface UserDataInterface {
  data: DataUser;
}

export interface UserFilterInterface {
  search: string;
  gender: E_GENDER;
}

export interface NameInterface {
  title: string;
  first: string;
  last: string;
}

interface RegisteredInterface {
  age: number;
  date: string;
}

export interface UserResponse {
  name: NameInterface;
  registered: RegisteredInterface;
  email: string;
  login: {
    username: string;
  };
  gender: string;
}

export interface UserFetchResponse {
  results: UserResponse[];
}
