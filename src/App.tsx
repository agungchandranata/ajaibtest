import React from "react";
import "./App.css";
import Example from "./container/Example";

const App = () => {
  return <Example />;
};

export default App;
