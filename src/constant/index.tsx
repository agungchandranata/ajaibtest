export enum E_GENDER {
  ALL = 0,
  MALE = 1,
  FEMALE = 2,
}

export enum E_FETCH_STATUS {
  INITIATE = 0,
  FETCHING = 1,
  FETCHED = 2,
  ERROR = 3,
}

export enum SORT_BY {
  NOTHING = 0,
  NAME = 1,
  EMAIL = 2,
  GENDER = 3,
  REGISTERED_DATE = 4,
}

export enum ORDER_BY {
  ASC = 1,
  DESC = 2,
}

export const BASE_URL = "https://randomuser.me/api/";
