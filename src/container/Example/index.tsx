import {
  CircularProgress,
  Container,
  Divider,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { FC, useEffect } from "react";
import BreadCrumb from "../../component/BreadCrumb";
import Filter from "../../component/Filter";
import {
  userListSelector,
  userLoadingSelector,
  userOrderBySelector,
  userSortBySelector,
} from "../../redux/selectors/user";
import { useSelector, useDispatch } from "react-redux";
import convertGenderToString from "../../utils/convertGenderToString";
import { E_FETCH_STATUS, ORDER_BY, SORT_BY } from "../../constant";
import ArrowDropUpSharpIcon from "@mui/icons-material/ArrowDropUpSharp";
import ArrowDropDownSharpIcon from "@mui/icons-material/ArrowDropDownSharp";
import "./index.scss";
import { DivHeaderTable, DivSort, HeaderTable } from "./styled";
import ExamplePagination from "../../component/ExamplePagination";
import { loadUserList, setOrderBy, setSortBy } from "../../redux/actions/user";

const Example: FC = () => {
  const dispatch = useDispatch();
  const userList = useSelector(userListSelector);
  const isLoading = useSelector(userLoadingSelector);
  const currentSortBy = useSelector(userSortBySelector);
  const currentOrderBy = useSelector(userOrderBySelector);

  const getUserTable = () => {
    return userList.map((user, index) => (
      <TableRow key={user.username}>
        <TableCell>{user.username}</TableCell>
        <TableCell>{user.name}</TableCell>
        <TableCell>{user.email}</TableCell>
        <TableCell>{convertGenderToString(user.gender)}</TableCell>
        <TableCell>{user.date}</TableCell>
      </TableRow>
    ));
  };

  const sortTable = (sortBy: SORT_BY) => {
    if (sortBy === currentSortBy) {
      if (currentSortBy === SORT_BY.NOTHING) {
        dispatch(setSortBy(SORT_BY.NAME));
      } else {
        if (currentOrderBy === ORDER_BY.ASC) {
          dispatch(setOrderBy(ORDER_BY.DESC));
        } else {
          dispatch(setOrderBy(ORDER_BY.ASC));
          dispatch(setSortBy(SORT_BY.NOTHING));
        }
      }
    } else {
      dispatch(setSortBy(sortBy));
      dispatch(setOrderBy(ORDER_BY.ASC));
    }
  };

  useEffect(() => {
    dispatch(loadUserList());
  }, [currentSortBy, currentOrderBy]);

  const renderTable = () => {
    let tableBody: JSX.Element | JSX.Element[];
    switch (isLoading) {
      case E_FETCH_STATUS.FETCHING:
        tableBody = (
          <TableRow>
            <TableCell colSpan={5} align="center">
              <CircularProgress />
            </TableCell>
          </TableRow>
        );
        break;
      case E_FETCH_STATUS.FETCHED:
        userList.length > 0
          ? (tableBody = getUserTable())
          : (tableBody = (
              <TableRow>
                <TableCell colSpan={5} align="center">
                  <p>No User Data</p>
                </TableCell>
              </TableRow>
            ));
        break;
      case E_FETCH_STATUS.ERROR:
        tableBody = (
          <TableRow>
            <TableCell colSpan={5} align="center">
              <p>Error Data Fetch</p>
            </TableCell>
          </TableRow>
        );
        break;
      default:
        tableBody = (
          <TableRow>
            <TableCell colSpan={5}>
              <p>No User Data</p>
            </TableCell>
          </TableRow>
        );
        break;
    }
    return (
      <TableContainer component={Paper}>
        <Table
          sx={{ minWidth: 700, tableLayout: "fixed" }}
          aria-label="customized table"
        >
          <TableHead>
            <TableRow>
              <TableCell className="custom-table-header">Username</TableCell>
              <TableCell
                className="custom-table-header"
                onClick={() => sortTable(SORT_BY.NAME)}
              >
                <DivHeaderTable>
                  <HeaderTable>Name</HeaderTable>
                  <DivSort>
                    <ArrowDropUpSharpIcon
                      color={
                        currentSortBy === SORT_BY.NAME &&
                        currentOrderBy === ORDER_BY.ASC
                          ? "primary"
                          : "inherit"
                      }
                    />
                    <ArrowDropDownSharpIcon
                      color={
                        currentSortBy === SORT_BY.NAME &&
                        currentOrderBy === ORDER_BY.DESC
                          ? "primary"
                          : "inherit"
                      }
                    />
                  </DivSort>
                </DivHeaderTable>
              </TableCell>
              <TableCell
                className="custom-table-header"
                onClick={() => sortTable(SORT_BY.EMAIL)}
              >
                <DivHeaderTable>
                  <HeaderTable>Email</HeaderTable>
                  <DivSort>
                    <ArrowDropUpSharpIcon
                      color={
                        currentSortBy === SORT_BY.EMAIL &&
                        currentOrderBy === ORDER_BY.ASC
                          ? "primary"
                          : "inherit"
                      }
                    />
                    <ArrowDropDownSharpIcon
                      color={
                        currentSortBy === SORT_BY.EMAIL &&
                        currentOrderBy === ORDER_BY.DESC
                          ? "primary"
                          : "inherit"
                      }
                    />
                  </DivSort>
                </DivHeaderTable>
              </TableCell>
              <TableCell
                className="custom-table-header"
                onClick={() => sortTable(SORT_BY.GENDER)}
              >
                <DivHeaderTable>
                  <HeaderTable>Gender</HeaderTable>
                  <DivSort>
                    <ArrowDropUpSharpIcon
                      color={
                        currentSortBy === SORT_BY.GENDER &&
                        currentOrderBy === ORDER_BY.ASC
                          ? "primary"
                          : "inherit"
                      }
                    />
                    <ArrowDropDownSharpIcon
                      color={
                        currentSortBy === SORT_BY.GENDER &&
                        currentOrderBy === ORDER_BY.DESC
                          ? "primary"
                          : "inherit"
                      }
                    />
                  </DivSort>
                </DivHeaderTable>
              </TableCell>
              <TableCell
                className="custom-table-header"
                onClick={() => sortTable(SORT_BY.REGISTERED_DATE)}
              >
                <DivHeaderTable>
                  <HeaderTable>Registered Date</HeaderTable>
                  <DivSort>
                    <ArrowDropUpSharpIcon
                      color={
                        currentSortBy === SORT_BY.REGISTERED_DATE &&
                        currentOrderBy === ORDER_BY.ASC
                          ? "primary"
                          : "inherit"
                      }
                    />
                    <ArrowDropDownSharpIcon
                      color={
                        currentSortBy === SORT_BY.REGISTERED_DATE &&
                        currentOrderBy === ORDER_BY.DESC
                          ? "primary"
                          : "inherit"
                      }
                    />
                  </DivSort>
                </DivHeaderTable>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{tableBody}</TableBody>
        </Table>
      </TableContainer>
    );
  };
  return (
    <Container>
      <BreadCrumb />
      <h1>Example With Search and Filter</h1>
      <Filter />
      <br />
      <Divider />
      <br />
      {renderTable()}
      <br />
      <ExamplePagination />
    </Container>
  );
};

export default Example;
