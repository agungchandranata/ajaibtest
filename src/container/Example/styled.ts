import styled from "styled-components";

export const DivHeaderTable = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const HeaderTable = styled.p`
  width: 90%;
  margin: 0;
`;

export const DivSort = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 10%;
`;
