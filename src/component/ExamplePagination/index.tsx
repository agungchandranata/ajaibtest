import { Pagination } from "@mui/material";
import React, { FC, useEffect } from "react";
import { userCurrentPageSelector } from "../../redux/selectors/user";
import { useSelector, useDispatch } from "react-redux";
import { loadUserList, setCurrentPage } from "../../redux/actions/user";
import "./index.scss";

const ExamplePagination: FC<{}> = () => {
  const dispatch = useDispatch();
  const currentPage = useSelector(userCurrentPageSelector);

  const handlePaginationChange = (e: any, page: number) => {
    dispatch(setCurrentPage(page));
  };

  useEffect(() => {
    dispatch(loadUserList());
  }, [currentPage]);
  return (
    <Pagination
      count={2}
      shape="rounded"
      variant="outlined"
      color="primary"
      className="pagination-flex-end"
      page={currentPage}
      onChange={(e, page) => handlePaginationChange(e, page)}
    />
  );
};

export default ExamplePagination;
