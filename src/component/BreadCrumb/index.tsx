import { Breadcrumbs, Link, Typography } from "@mui/material";
import React, { FC } from "react";

const BreadCrumb: FC<{}> = () => {
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Link color="inherit" underline="none">
        Home
      </Link>
      <Typography color="text.primary">Example</Typography>
    </Breadcrumbs>
  );
};

export default BreadCrumb;
