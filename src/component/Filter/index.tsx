import {
  Grid,
  IconButton,
  TextField,
  MenuItem,
  Select,
  Button,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import React, { FC, useEffect, useState } from "react";
import { userFilterSelector } from "../../redux/selectors/user";
import { useDispatch, useSelector } from "react-redux";
import { loadUserList, setUserFilter } from "../../redux/actions/user";
import { E_GENDER } from "../../constant";
import { UserFilterInterface } from "../../interfaces/User";

const Filter: FC = () => {
  const dispatch = useDispatch();
  const search = useSelector(userFilterSelector);
  const [searchState, setSearchState] = useState<string>("");

  const handleGenderChange = (e: any) => {
    const filterTemp: UserFilterInterface = {
      search: search.search,
      gender: e.target.value,
    };
    dispatch(setUserFilter(filterTemp));
  };

  const handleSearchChange = (e: any) => {
    setSearchState(e.target.value);
  };

  const handleSearchClick = () => {
    const filterTemp: UserFilterInterface = {
      search: searchState,
      gender: search.gender,
    };
    dispatch(setUserFilter(filterTemp));
  };

  const resetFilter = () => {
    setSearchState("");
    const filterTemp: UserFilterInterface = {
      search: "",
      gender: E_GENDER.ALL,
    };
    dispatch(setUserFilter(filterTemp));
  };

  useEffect(() => {
    dispatch(loadUserList());
  }, [search]);

  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          handleSearchClick();
        }}
      >
        <Grid container spacing={2} sx={{ alignItems: "flex-end" }}>
          <Grid item xs={2}>
            <h3>Search</h3>
            <TextField
              size="small"
              placeholder="Search..."
              onChange={(e) => handleSearchChange(e)}
              onBlur={handleSearchClick}
              value={searchState}
              fullWidth
              InputProps={{
                endAdornment: (
                  <IconButton
                    edge="end"
                    color="primary"
                    type="submit"
                    onClick={handleSearchClick}
                  >
                    <SearchIcon />
                  </IconButton>
                ),
              }}
            />
          </Grid>
          <Grid item xs={2}>
            <h3>Gender</h3>
            <Select
              size="small"
              value={search.gender}
              fullWidth
              onChange={(e) => handleGenderChange(e)}
              //   displayEmpty
            >
              <MenuItem value={E_GENDER.ALL}>All</MenuItem>
              <MenuItem value={E_GENDER.MALE}>Male</MenuItem>
              <MenuItem value={E_GENDER.FEMALE}>Female</MenuItem>
            </Select>
          </Grid>
          <Grid item xs={2}>
            <Button variant="outlined" onClick={resetFilter} size="large">
              Reset Filter
            </Button>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default Filter;
