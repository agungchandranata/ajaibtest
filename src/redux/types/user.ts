import { E_FETCH_STATUS, E_GENDER, ORDER_BY, SORT_BY } from "../../constant";
import { DataUser } from "../../interfaces/User";

export enum UserType {
  USER_SET_LIST = "USER_SET_LIST",
  USER_SET_FILTER = "USER_SET_FILTER",
  USER_SET_LOADING = "USER_SET_LOADING",
  USER_SET_PAGE_NUMBER = "USER_SET_PAGE_NUMBER",
  USER_SET_CURRENT_PAGE_NUMBER = "USER_SET_CURRENT_PAGE_NUMBER",
  USER_SET_SORT_BY = "USER_SET_SORT_BY",
  USER_SET_ORDER_BY = "USER_SET_ORDER_BY",
}

export interface UserSetList {
  type: UserType.USER_SET_LIST;
  payload: DataUser[];
}

export interface UserSetLoading {
  type: UserType.USER_SET_LOADING;
  payload: E_FETCH_STATUS;
}

export interface UserSetPageNumber {
  type: UserType.USER_SET_PAGE_NUMBER;
  payload: number;
}

export interface UserSetCurrentPageNumber {
  type: UserType.USER_SET_CURRENT_PAGE_NUMBER;
  payload: number;
}

export interface UserSetFilter {
  type: UserType.USER_SET_FILTER;
  payload: {
    search: string;
    gender: E_GENDER;
  };
}

export interface UserSetSortBy {
  type: UserType.USER_SET_SORT_BY;
  payload: SORT_BY;
}

export interface UserSetOrderBy {
  type: UserType.USER_SET_ORDER_BY;
  payload: ORDER_BY;
}

export type UserAction =
  | UserSetList
  | UserSetFilter
  | UserSetLoading
  | UserSetPageNumber
  | UserSetCurrentPageNumber
  | UserSetOrderBy
  | UserSetSortBy;
