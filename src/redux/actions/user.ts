import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { E_FETCH_STATUS, E_GENDER, ORDER_BY, SORT_BY } from "../../constant";
import {
  DataUser,
  NameInterface,
  UserFilterInterface,
} from "../../interfaces/User";
import UserService from "../../service/User";
import { AppStateType } from "../reducers";
import { stores } from "../stores";
import { UserAction, UserType } from "../types/user";
import { UserFetchResponse } from "../../interfaces/User";
import convertGenderToString from "../../utils/convertGenderToString";

const concatDataName = (name: NameInterface) => {
  return `${name.first} ${name.last}`;
};

const convertGenderToEnum = (gender: string) => {
  switch (gender) {
    case "male":
      return E_GENDER.MALE;
    case "female":
      return E_GENDER.FEMALE;
    default:
      return;
  }
};

const convertSortByToString = (sort: SORT_BY) => {
  switch (sort) {
    case SORT_BY.NAME:
      return "name";
    case SORT_BY.EMAIL:
      return "email";
    case SORT_BY.GENDER:
      return "gender";
    case SORT_BY.REGISTERED_DATE:
      return "date";
    default:
      break;
  }
};

const convertOrderByToString = (order: ORDER_BY) => {
  switch (order) {
    case ORDER_BY.ASC:
      return "ascend";
    case ORDER_BY.DESC:
      return "descend";
    default:
      break;
  }
};

const getRealDate = (date: string) => {
  const dateObj = new Date(date);
  const year = dateObj.getFullYear();
  const month = `0${dateObj.getMonth()}`.slice(-2);
  const realDate = `0${dateObj.getDate()}`.slice(-2);
  const hour = `0${dateObj.getHours()}`.slice(-2);
  const minute = `0${dateObj.getMinutes()}`.slice(-2);
  return `${realDate}-${month}-${year} ${hour}:${minute}`;
};

export const setUserFilter = (filter: UserFilterInterface): UserAction => {
  return {
    type: UserType.USER_SET_FILTER,
    payload: filter,
  };
};

export const setUserLoading = (status: E_FETCH_STATUS) => {
  return {
    type: UserType.USER_SET_LOADING,
    payload: status,
  };
};

export const setUserList = (userList: DataUser[]) => {
  return {
    type: UserType.USER_SET_LIST,
    payload: userList,
  };
};

export const setUserTotalPage = (totalPage: number) => {
  return {
    type: UserType.USER_SET_PAGE_NUMBER,
    payload: totalPage,
  };
};

export const setCurrentPage = (page: number) => {
  return {
    type: UserType.USER_SET_CURRENT_PAGE_NUMBER,
    payload: page,
  };
};

export const setSortBy = (sortBy: SORT_BY) => {
  return {
    type: UserType.USER_SET_SORT_BY,
    payload: sortBy,
  };
};

export const setOrderBy = (orderBy: ORDER_BY) => {
  return {
    type: UserType.USER_SET_ORDER_BY,
    payload: orderBy,
  };
};

export const loadUserList = () => {
  return async (dispatch: ThunkDispatch<AppStateType, null, AnyAction>) => {
    try {
      dispatch(setUserLoading(E_FETCH_STATUS.FETCHING));
      const reduxState = stores.getState();
      const {
        user: { filter, currentPage, sortBy, orderBy },
      } = reduxState;

      let params = `?page=${currentPage}&pageSize=10&results=10`;
      if (filter.search !== "") {
        params += `&keyword=${filter.search}`;
      }
      if (filter.gender !== E_GENDER.ALL) {
        params += `&gender=${convertGenderToString(filter.gender)}`;
      }
      if (sortBy !== SORT_BY.NOTHING) {
        params += `&sortBy=${convertSortByToString(
          sortBy
        )}&sortOrder=${convertOrderByToString(orderBy)}`;
      }
      const res: UserFetchResponse = await UserService.getList(params);
      if (res.results) {
        console.log(new Date(res.results[0].registered.date));
        const resultTemp: DataUser[] = res.results.map((data) => {
          return {
            username: data.login.username,
            name: concatDataName(data.name),
            email: data.email,
            gender: convertGenderToEnum(data.gender) as E_GENDER,
            date: getRealDate(data.registered.date),
          };
        });
        dispatch(setUserList(resultTemp));
        dispatch(setUserLoading(E_FETCH_STATUS.FETCHED));
      } else {
        dispatch(setUserLoading(E_FETCH_STATUS.ERROR));
      }
    } catch (e: any) {
      dispatch(setUserLoading(E_FETCH_STATUS.ERROR));
      throw new Error(e);
    }
  };
};
