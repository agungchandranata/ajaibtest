import { E_FETCH_STATUS, E_GENDER, ORDER_BY, SORT_BY } from "../../constant";
import { DataUser, UserFilterInterface } from "../../interfaces/User";
import {
  UserAction,
  UserSetCurrentPageNumber,
  UserSetFilter,
  UserSetList,
  UserSetLoading,
  UserSetOrderBy,
  UserSetPageNumber,
  UserSetSortBy,
  UserType,
} from "../types/user";

interface UserState {
  filter: UserFilterInterface;
  orderBy: ORDER_BY;
  sortBy: SORT_BY;
  userList: DataUser[];
  isLoading: E_FETCH_STATUS;
  totalPage: number;
  currentPage: number;
}

const initialState: UserState = {
  filter: {
    search: "",
    gender: E_GENDER.ALL,
  },
  orderBy: ORDER_BY.ASC,
  sortBy: SORT_BY.NOTHING,
  userList: [],
  isLoading: E_FETCH_STATUS.INITIATE,
  totalPage: 0,
  currentPage: 1,
};

const {
  USER_SET_FILTER,
  USER_SET_LIST,
  USER_SET_LOADING,
  USER_SET_PAGE_NUMBER,
  USER_SET_CURRENT_PAGE_NUMBER,
  USER_SET_SORT_BY,
  USER_SET_ORDER_BY,
} = UserType;

const user = (
  state: UserState = initialState,
  action: UserAction
): UserState => {
  const { type } = action;

  const setList = (): UserState => {
    const { payload: userList } = action as UserSetList;
    return {
      ...state,
      userList,
    };
  };

  const setUserLoading = (): UserState => {
    const { payload: isLoading } = action as UserSetLoading;
    return {
      ...state,
      isLoading,
    };
  };

  const setFilter = (): UserState => {
    const { payload: filter } = action as UserSetFilter;
    return {
      ...state,
      filter,
    };
  };

  const setPageNumber = (): UserState => {
    const { payload: totalPage } = action as UserSetPageNumber;
    return {
      ...state,
      totalPage,
    };
  };

  const setCurrentPageNumber = (): UserState => {
    const { payload: currentPage } = action as UserSetCurrentPageNumber;
    return {
      ...state,
      currentPage,
    };
  };

  const setSortBy = (): UserState => {
    const { payload: sortBy } = action as UserSetSortBy;
    return {
      ...state,
      sortBy,
    };
  };

  const setOrderBy = (): UserState => {
    const { payload: orderBy } = action as UserSetOrderBy;
    return {
      ...state,
      orderBy,
    };
  };

  switch (type) {
    case USER_SET_FILTER:
      return setFilter();
    case USER_SET_LOADING:
      return setUserLoading();
    case USER_SET_LIST:
      return setList();
    case USER_SET_PAGE_NUMBER:
      return setPageNumber();
    case USER_SET_CURRENT_PAGE_NUMBER:
      return setCurrentPageNumber();
    case USER_SET_SORT_BY:
      return setSortBy();
    case USER_SET_ORDER_BY:
      return setOrderBy();
    default:
      return state;
  }
};

export default user;
