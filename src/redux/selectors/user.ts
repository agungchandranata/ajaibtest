import { AppStateType } from "../reducers";

export const userFilterSelector = (state: AppStateType) => {
  return state.user.filter;
};

export const userLoadingSelector = (state: AppStateType) => {
  return state.user.isLoading;
};

export const userListSelector = (state: AppStateType) => {
  return state.user.userList;
};

export const userTotalPageSelector = (state: AppStateType) => {
  return state.user.totalPage;
};

export const userCurrentPageSelector = (state: AppStateType) => {
  return state.user.currentPage;
};

export const userSortBySelector = (state: AppStateType) => {
  return state.user.sortBy;
};

export const userOrderBySelector = (state: AppStateType) => {
  return state.user.orderBy;
};
