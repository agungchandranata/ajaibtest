import { E_GENDER } from "../constant";

const convertGenderToString = (gender: E_GENDER) => {
  switch (gender) {
    case E_GENDER.MALE:
      return "male";
    case E_GENDER.FEMALE:
      return "female";
    default:
      return "transgender";
  }
};

export default convertGenderToString;
